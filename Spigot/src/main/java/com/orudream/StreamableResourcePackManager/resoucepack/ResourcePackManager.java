package com.orudream.StreamableResourcePackManager.resoucepack;

import com.orudream.StreamableResourcePackManager.StreamableResourcePackManager;
import com.orudream.StreamableResourcePackManager.config.ConfigManager;
import com.orudream.StreamableResourcePackManager.httpd.MineHttpd;
import com.orudream.StreamableResourcePackManager.util.Util;
import com.orudream.StreamableResourcePackManager.util.WebUtil;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.io.File;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.*;

public class ResourcePackManager {

    private static final Integer MAX_RETRIES_PER_PLAYER = 5;
    protected Map<String, File> resourcepacks;
    StreamableResourcePackManager plugin;
    ConfigManager configManager;
    private MineHttpd httpd;
    private Map<UUID, Integer> playerSuggestedResourcePackRetriesCount = new HashMap<>();

    public ResourcePackManager(StreamableResourcePackManager plugin, ConfigManager configManager) {
        this.plugin = plugin;
        this.configManager = configManager;
        this.resourcepacks = this.configManager.resourcepacks;
        plugin.getLogger().info("Machine public IP is '" + configManager.ip + "'");
    }

    /**
     * Send a resource pack to the desired player
     *
     * @param player       The player to send it to, note that they must have the right permission to download the pack
     * @param resourcepack The id of the resourcepack to send; must be valid
     */
    public void sendResourcePack(Player player, String resourcepack) {
        File file = resourcepacks.get(resourcepack);
        try {
            player.setResourcePack("http://" + configManager.ip + ":" + configManager.port + "/" + resourcepack, Util.calcSHA1(file));
        } catch (NoSuchAlgorithmException | IOException e) {
            e.printStackTrace();
            if (resourcepack.equalsIgnoreCase(configManager.sendPackOnJoin_packId)) {
                player.setInvulnerable(false);
            }
        }
    }

    public void startHttpd() {
        try {
            httpd = new MineHttpd(configManager.port) {
                @Override
                public File requestFileCallback(MineConnection connection, String request) {
                    Player player = WebUtil.getAddress(connection);
                    if (player == null) {
                        plugin.verbose("Unknown connection from '" + connection.getClient().getInetAddress() + "'. Aborting...");
                        return null;
                    }
                    if (!resourcepacks.containsKey(request)) {
                        return null;
                    }
                    plugin.verbose("Serving '" + request + "' to " + player.getName() + "(" + connection.getClient().getInetAddress() + ")");
                    return resourcepacks.get(request);
                }

                public void onSuccessfulRequest(MineConnection connection, String request) {
                    plugin.verbose("Successfully served '" + request + "' to " + connection.getClient().getInetAddress());
                }

                public void onClientRequest(MineConnection connection, String request) {
                    plugin.verbose("Request '" + request + "' recieved from " + connection.getClient().getInetAddress());
                }

                public void onRequestError(MineConnection connection, int code) {
                    plugin.verbose("Error " + code + " when attempting to serve " + connection.getClient().getInetAddress());
                }
            };
            // Start the web server
            httpd.start();
            plugin.getLogger().info(ChatColor.GREEN + "Successfully started the mini http daemon!");
        } catch (IOException e1) {
            plugin.getLogger().severe(ChatColor.RED + "Unable to start the mini http daemon! Disabling...");
            Bukkit.getPluginManager().disablePlugin(plugin);
        }
    }

    public void stopHttpd() {
        httpd.terminate();
    }

    public void sendSuggestedResourcePack(Player player) {
        player.setInvulnerable(true);
        try {
            UUID playerUUID = player.getUniqueId();
            if (!playerSuggestedResourcePackRetriesCount.containsKey(playerUUID)) {
                playerSuggestedResourcePackRetriesCount.put(playerUUID, 1);
            }
            Integer playerRetriesCount = playerSuggestedResourcePackRetriesCount.get(playerUUID);
            if (playerRetriesCount < MAX_RETRIES_PER_PLAYER) {
                playerSuggestedResourcePackRetriesCount.put(playerUUID, ++playerRetriesCount);
            } else {
                playerSuggestedResourcePackRetriesCount.remove(playerUUID);
            }
            sendResourcePack(player, configManager.sendPackOnJoin_packId);
        } catch (Exception ex) {
            ex.printStackTrace();
            player.setInvulnerable(false);
        }
    }

    public void stopSendingSuggestedResourcePack(UUID uniqueId) {
        if (playerSuggestedResourcePackRetriesCount.containsKey(uniqueId)) {
            playerSuggestedResourcePackRetriesCount.remove(uniqueId);
        }
    }

    public boolean isPlayerOnSuggestedResourcePackRetryList(UUID uniqueId) {
        return playerSuggestedResourcePackRetriesCount.containsKey(uniqueId);
    }

    public boolean isResourcePackLoaded(String resourcePackName) {
        return resourcepacks.containsKey(resourcePackName);
    }

    public List<String> getResourcePackIds() {
        return new ArrayList<>(resourcepacks.keySet());
    }

}
