package com.orudream.StreamableResourcePackManager.commands;

import com.orudream.StreamableResourcePackManager.StreamableResourcePackManager;
import com.orudream.StreamableResourcePackManager.resoucepack.ResourcePackManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;
import org.bukkit.util.StringUtil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Commands implements CommandExecutor, TabCompleter {
    private StreamableResourcePackManager plugin;
    private ResourcePackManager resourcePackManager;

    public Commands(StreamableResourcePackManager plugin, ResourcePackManager resourcePackManager) {
        this.plugin = plugin;
        this.resourcePackManager = resourcePackManager;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (args.length == 0) {
            showHelp(sender);
            return false;
        } else if (args.length == 1) {
            if (args[0].equalsIgnoreCase("retry")) {
                if (sender instanceof Player) {
                    resourcePackManager.sendSuggestedResourcePack((Player) sender);
                } else {
                    sender.sendMessage(ChatColor.RED + "You need to be a player to run this command!");
                }
            } else {
                showHelp(sender);
            }
            return false;
        } else if (args.length == 2) {
            if (args[0].equalsIgnoreCase("load")) {
                String packId = args[1];
                if (sender.hasPermission("srpm.user") || sender.hasPermission("srpm.admin") || sender.hasPermission(String.format("srpm.pack.%s", packId))) {
                    if (sender instanceof Player) {

                        if (resourcePackManager.isResourcePackLoaded(packId)) {
                            resourcePackManager.sendResourcePack((Player) sender, packId);
                        } else {
                            sender.sendMessage(ChatColor.RED + "Resource pack not found!");
                        }
                    } else {
                        sender.sendMessage(ChatColor.RED + "You need to be a player to run this command!");
                    }
                } else {
                    sender.sendMessage(ChatColor.RED + "You do not have permission to run this command!");
                }
            } else {
                showHelp(sender);
            }
        } else if (args.length == 3) {
            if (args[0].equalsIgnoreCase("send")) {
                if (sender.hasPermission("srpm.admin")) {
                    String packId = args[1];
                    Player player = Bukkit.getPlayer(args[2]);
                    if (player == null) {
                        sender.sendMessage(ChatColor.RED + args[2] + " is not online right now!");
                    } else {
                        if (resourcePackManager.isResourcePackLoaded(packId)) {
                            resourcePackManager.sendResourcePack(player, packId);
                            sender.sendMessage(ChatColor.GREEN + "Sent '" + packId + "' to " + player.getName());
                        }
                    }
                } else {
                    sender.sendMessage(ChatColor.RED + "You do not have permission to run this command!");
                }
            }
        } else {
            showHelp(sender);
            return false;
        }
        return false;
    }


    private void showHelp(CommandSender sender) {
        if (sender.hasPermission("srpm.admin") || sender.hasPermission("srpm.user")) {
            sender.sendMessage(ChatColor.GOLD + "-------------------------------------");
            sender.sendMessage(ChatColor.GOLD + "     StreamableResourcePackManager");
            sender.sendMessage(ChatColor.GOLD + "-------------------------------------");
        }
        if (sender.hasPermission("srpm.admin")) {
            sender.sendMessage(ChatColor.AQUA + "/resourcepack send <id> [player]" + ChatColor.RESET + " - Allows server admins or the console to prompt the desired player with a custom resource pack.");
        } else if (sender.hasPermission("srpm.user")) {
            sender.sendMessage(ChatColor.AQUA + "/resourcepack retry" + ChatColor.RESET + " - Lets players retry to download the onJoinPack.");
            sender.sendMessage(ChatColor.AQUA + "/resourcepack load <id>"+ ChatColor.RESET + " - Lets players download resource packs they have access to.");
        } else {
            sender.sendMessage(ChatColor.RED + "You do not have permission to run this command!");
        }
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
        List<String> aos = new ArrayList<>();
        List<String> completions = new ArrayList<>();

        if (args.length == 1) {
            if (sender.hasPermission("srpm.admin") || sender.hasPermission("srpm.user")) {
                aos.add("retry");
                aos.add("load");
            }
            if (sender.hasPermission("srpm.admin")) {
                aos.add("send");
            }
        } else if (args.length == 2) {
            if (sender.hasPermission("srpm.admin") || sender.hasPermission("srpm.user")) {
                for (String packId : resourcePackManager.getResourcePackIds()) {
                    if (sender.hasPermission(String.format("srpm.pack.%s", packId))) {
                        aos.add(packId);
                    }
                }
            }
        }
        Collections.sort(completions);
        return StringUtil.copyPartialMatches(args[args.length - 1], aos, completions);
    }

}
