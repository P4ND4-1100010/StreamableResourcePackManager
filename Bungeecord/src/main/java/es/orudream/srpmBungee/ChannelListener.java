package es.orudream.srpmBungee;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.connection.Server;
import net.md_5.bungee.api.event.PostLoginEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

import java.util.Collection;
import java.util.concurrent.TimeUnit;

public class ChannelListener implements Listener {

    @EventHandler
    public void onPluginMessage(PostLoginEvent e) {
        ProxiedPlayer player = e.getPlayer();
        sendCustomData(player, "onJoin", player.getUniqueId().toString());
    }

    private void sendCustomData(ProxiedPlayer player, String subChannel, String data1) {
        Collection<ProxiedPlayer> networkPlayers = ProxyServer.getInstance().getPlayers();
        // perform a check to see if globally are no players
        if (networkPlayers == null || networkPlayers.isEmpty()) {
            return;
        } else {
            ByteArrayDataOutput out = ByteStreams.newDataOutput();
            out.writeUTF(subChannel);
            out.writeUTF(data1);
            ProxyServer.getInstance().getScheduler().schedule(StreamableResourcePackManagerBungee.getInstance(), new Runnable() {
                public void run() {
                    Server server = player.getServer();
                    if(server != null) {
                        server.getInfo().sendData(StreamableResourcePackManagerBungee.CHANNEL_NAME,
                                out.toByteArray());
                    }
                }
            }, 1, TimeUnit.SECONDS);
        }
    }

}
