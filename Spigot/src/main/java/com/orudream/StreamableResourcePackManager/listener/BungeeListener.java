package com.orudream.StreamableResourcePackManager.listener;

import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteStreams;
import com.orudream.StreamableResourcePackManager.StreamableResourcePackManager;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.messaging.PluginMessageListener;

import java.util.UUID;

public class BungeeListener implements PluginMessageListener {

    StreamableResourcePackManager plugin;

    public BungeeListener(StreamableResourcePackManager instance) {
        this.plugin = instance;
    }

    @Override
    public void onPluginMessageReceived(String channel, Player sender, byte[] message) {
        if (channel.equalsIgnoreCase(plugin.CHANNEL_NAME)) {
            ByteArrayDataInput in = ByteStreams.newDataInput(message);
            String subChannel = in.readUTF();
            if (subChannel.equalsIgnoreCase("onJoin")) {
                String data1 = in.readUTF();
                Player player = Bukkit.getPlayer(UUID.fromString(data1));
                if (player != null && !player.hasPermission("srpm.bypassOnJoinPack")) {
                    plugin.getResourcePackManager().sendSuggestedResourcePack(player);
                }
            }
        }
    }

}
