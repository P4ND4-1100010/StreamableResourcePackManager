# **StreamableResourcePackManager**
### What is this?
StreamableResourcePackManager allows your server to self-serve resource pack requests. An additional port is required for the mini http daemon used to handle client requests.

### Cool, so how does this work?
StreamableResourcePackManager hosts a mini http daemon which listens for client requests. Once it recieves a request, it will check if the ip of the client requesting matches an online player's ip. If so, it will send it to the player.

### How secure is it?
The mini http daemon only serves resource packs inside the config, and no more than that. Premium resource packs are safe and can only be accessed by people with the right permissions, since the ip of the request and player are checked so that it's not possible to request a resource pack from a computer that is not connected to the server.

### Commands
* **/resourcepack load {IdPack}** - Lets players download resource packs they have access to.
* **/resourcepack send {IdPack} {PlayerName}** - Allows server admins or the console to prompt the desired player with a custom resource pack.
* **/resourcepack retry** - Lets players retry to download the onJoinPack.

### Permissions
* srpm.user - True by default; lets players change their own resource pack.
* srpm.admin - OP by default; lets admins set players' resource packs. The desired player must have permission to use the resource pack specified.
* srpm.pack.{id} - False by default; Allow players to download the resource pack (Has no effect onJoinPack).
* srpm.bypassOnJoinPack - Lets players bypass the onJoinPack

### The Config
Requires a restart to update in-game.  
```YAML
# The port to use for the mini-http daemon
# IMPORTANT: Open the port in your router and firewall (TCP)
port: 60765
# Whether or not the console should print a message whenever a player attempts to fetch a resource pack
verbose: false
# Whether or not the server is localhost; Useful for testing situations if you're not able to open a port
localhost: false
# ResourcePack maping
# IMPORTANT: The packs should be placed inside the '/plugins/StreamableResourcePackManager/resourcepacks/' folder
# Format to follow:
# packs:
#   <id1>: <filename1>
#   <id2>: <filename2>
packs:
  testPack: testResourcePack.zip
# Whether or not send a ResourcePack on player join
sendPackOnJoin:
  enabled: true
  packId: testPack
  retryMessage: "Retrying to download..."
events:
  onAccept:
    message: "Wise choice"
    commands:
    - say [player] accepted the resource pack!
  onSuccessfulLoad:
    message: "Right choice"
    commands:
    - say [player] accepted the resource pack!
  onDecline:
    message: ">:C!"
    kick:
      enabled: true
      message: "Please accept the resource pack :C"
    commands:
    - say [player] denied the resource pack!
  onFail:
    message: "Failed to download the resource pack! Try to run: '/resourcepack retry' or contact the staff"
    commands:
```

### Authors
- P4ND4_1100010

### Forked from:
https://github.com/Aeternum-Studios/LocalResourcePackHoster