package es.orudream.srpmBungee;

import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.api.plugin.Plugin;


public class StreamableResourcePackManagerBungee extends Plugin implements Listener {

    private static StreamableResourcePackManagerBungee instance;
    public static final String CHANNEL_NAME = "orudream:srpm";

    public static StreamableResourcePackManagerBungee getInstance() {
        return instance;
    }

    @Override
    public void onEnable() {
        instance = this;
        getProxy().registerChannel(CHANNEL_NAME);
        getProxy().getPluginManager().registerListener(this, new ChannelListener());
        getLogger().info("Communication channel registered correctly");
    }

}
