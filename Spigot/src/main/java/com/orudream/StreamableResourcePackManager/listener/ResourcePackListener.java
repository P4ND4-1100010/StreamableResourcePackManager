package com.orudream.StreamableResourcePackManager.listener;

import com.orudream.StreamableResourcePackManager.StreamableResourcePackManager;
import com.orudream.StreamableResourcePackManager.config.ConfigManager;
import com.orudream.StreamableResourcePackManager.resoucepack.ResourcePackManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerResourcePackStatusEvent;

public class ResourcePackListener implements Listener {

    StreamableResourcePackManager plugin;
    ResourcePackManager resourcePackManager;
    ConfigManager configManager;


    public ResourcePackListener(StreamableResourcePackManager plugin, ResourcePackManager resourcePackManager, ConfigManager configManager) {
        this.plugin = plugin;
        this.resourcePackManager = resourcePackManager;
        this.configManager = configManager;
    }

    @EventHandler
    public void ResourcePackStatus(PlayerResourcePackStatusEvent prpse) {
        Player player = prpse.getPlayer();
        String playerMessage = "";
        switch (prpse.getStatus()) {

            case ACCEPTED:
                plugin.getLogger().info(player.getName() + " accepted the resource pack.");
                playerMessage = plugin.getConfig().getString("events.onAccept.message").trim();
                if (!playerMessage.equals("")) {
                    player.sendMessage(ChatColor.translateAlternateColorCodes('&',playerMessage));
                }
                for (String cmd : plugin.getConfig().getStringList("events.onAccept.commands")) {
                    Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), cmd.replace("[player]", player.getName()));
                }
                break;

            case SUCCESSFULLY_LOADED:
                player.setInvulnerable(false);
                resourcePackManager.stopSendingSuggestedResourcePack(player.getUniqueId());
                plugin.getLogger().info(player.getName() + " successfully loaded the resource pack.");
                playerMessage = plugin.getConfig().getString("events.onSuccessfulLoad.message").trim();
                if (!playerMessage.equals("")) {
                    player.sendMessage(ChatColor.translateAlternateColorCodes('&',playerMessage));
                }
                for (String cmd : plugin.getConfig().getStringList("events.onSuccessfulLoad.commands")) {
                    Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), cmd.replace("[player]", player.getName()));
                }
                break;

            case DECLINED:
                player.setInvulnerable(false);
                resourcePackManager.stopSendingSuggestedResourcePack(player.getUniqueId());
                plugin.getLogger().info(player.getName() + " declined the resource pack.");
                playerMessage = plugin.getConfig().getString("events.onDecline.message").trim();
                if (!playerMessage.equals("")) {
                    player.sendMessage(ChatColor.translateAlternateColorCodes('&',playerMessage));
                }
                for (String cmd : plugin.getConfig().getStringList("events.onDecline.commands")) {
                    Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), cmd.replace("[player]", player.getName()));
                }
                if (plugin.getConfig().getBoolean("events.onDecline.kick.enabled")) {
                    player.kickPlayer(ChatColor.translateAlternateColorCodes('&',
                            plugin.getConfig().getString("events.onDecline.kick.message")));
                }
                break;

            case FAILED_DOWNLOAD:
                player.setInvulnerable(false);
                plugin.getLogger().info(player.getName() + " failed to download the resource pack.");
                playerMessage = plugin.getConfig().getString("events.onFail.message").trim();
                if (!playerMessage.equals("")) {
                    player.sendMessage(ChatColor.translateAlternateColorCodes('&',playerMessage));
                }
                for (String cmd : plugin.getConfig().getStringList("events.onFail.commands")) {
                    Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), cmd.replace("[player]", player.getName()));
                }
                if (resourcePackManager.isPlayerOnSuggestedResourcePackRetryList(player.getUniqueId())) {
                    resourcePackManager.sendSuggestedResourcePack(player);
                    player.sendMessage(ChatColor.translateAlternateColorCodes('&',
                            plugin.getConfig().getString("sendPackOnJoin.retryMessage")));
                }
                break;
        }
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent pje) {
        Player player = pje.getPlayer();
        if (!player.hasPermission("srpm.bypassOnJoinPack") && !plugin.isBungeeEnabled()) {
            resourcePackManager.sendSuggestedResourcePack(player);
        }
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent pqe) {
        Player player = pqe.getPlayer();
        if (!player.hasPermission("srpm.bypassOnJoinPack")) {
            resourcePackManager.stopSendingSuggestedResourcePack(player.getUniqueId());
        }
    }
}
