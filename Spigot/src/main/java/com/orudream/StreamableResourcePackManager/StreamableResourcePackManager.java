package com.orudream.StreamableResourcePackManager;

import com.orudream.StreamableResourcePackManager.commands.Commands;
import com.orudream.StreamableResourcePackManager.config.ConfigManager;
import com.orudream.StreamableResourcePackManager.listener.BungeeListener;
import com.orudream.StreamableResourcePackManager.listener.ResourcePackListener;
import com.orudream.StreamableResourcePackManager.resoucepack.ResourcePackManager;
import com.orudream.StreamableResourcePackManager.util.FileUtil;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;


public class StreamableResourcePackManager extends JavaPlugin {

    public static final String CHANNEL_NAME = "orudream:srpm";
    private static StreamableResourcePackManager plugin;
    private static ResourcePackManager resourcePackManager;
    private static ConfigManager configManager;
    private boolean isBungee = false;

    public ResourcePackManager getResourcePackManager() {
        return resourcePackManager;
    }

    @Override
    public void onEnable() {
        plugin = this;
        checkIfBungeeIsEnabled();
        if (isBungee) {
            getServer().getMessenger().registerIncomingPluginChannel(this, CHANNEL_NAME, new BungeeListener(this));
            getLogger().info("Communication channel registered correctly");
        }
        saveResource("README.md", true);
        if (!new File(getDataFolder(), "config.yml").exists()) {
            getLogger().info(ChatColor.GREEN + "Detected a fresh install! Please be sure to read the README.md provided!");
            FileUtil.saveToFile(getResource("data/testResourcePack.zip"), new File(getDataFolder() + "/resourcepacks", "testResourcePack.zip"), false);
        }
        configManager = new ConfigManager(plugin);

        new File(getDataFolder(), "resourcepacks").mkdirs();
        resourcePackManager = new ResourcePackManager(plugin, configManager);
        Commands command = new Commands(this, resourcePackManager);
        getCommand("resourcepack").setExecutor(command);
        getCommand("resourcepack").setTabCompleter(command);
        resourcePackManager.startHttpd();
        registerListeners();
    }

    @Override
    public void onDisable() {
        resourcePackManager.stopHttpd();
    }

    private void registerListeners() {
        PluginManager pm = Bukkit.getPluginManager();
        pm.registerEvents(new ResourcePackListener(plugin, resourcePackManager, configManager), plugin);
    }

    public void verbose(Object object) {
        if (configManager.verbose) {
            getLogger().info(object.toString());
        }
    }

    public boolean isBungeeEnabled() {
        return isBungee;
    }
    private void checkIfBungeeIsEnabled() {
        // we check if the server is Spigot/Paper (because of the spigot.yml file)
        if (!getServer().getVersion().contains("Spigot") && !getServer().getVersion().contains("Paper")) {
            isBungee = false;
        } else {
            isBungee = (getServer().spigot().getConfig().getConfigurationSection("settings").getBoolean("bungeecord"));
        }
    }
}
