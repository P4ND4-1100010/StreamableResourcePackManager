package com.orudream.StreamableResourcePackManager.config;

import com.orudream.StreamableResourcePackManager.StreamableResourcePackManager;
import com.orudream.StreamableResourcePackManager.util.BukkitUtil;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class ConfigManager {

    public boolean verbose = false;
    public boolean localhost = false;
    public String ip = "";
    public int port = 0;
    public Map<String, File> resourcepacks = new HashMap<>();
    public String sendPackOnJoin_packId = "";
    private StreamableResourcePackManager plugin;

    public ConfigManager(StreamableResourcePackManager plugin) {
        this.plugin = plugin;
        createConfig();
        loadConfig();
    }

    private void createConfig() {
        plugin.saveDefaultConfig();
    }

    private void loadConfig() {
        FileConfiguration config = YamlConfiguration.loadConfiguration(new File(plugin.getDataFolder(), "config.yml"));

        verbose = config.getBoolean("verbose");
        localhost = config.getBoolean("localhost");
        ip = localhost ? Bukkit.getIp() : BukkitUtil.getIp();
        port = plugin.getConfig().getInt("port");
        sendPackOnJoin_packId = plugin.getConfig().getString("sendPackOnJoin.packId");

        if (config.contains("packs")) {

            for (String key : config.getConfigurationSection("packs").getKeys(false)) {
                String name = config.getString("packs." + key);
                File file = new File(plugin.getDataFolder() + "/resourcepacks", name);
                if (!file.exists()) {
                    plugin.getLogger().severe(ChatColor.RED + "Resource pack '" + name + "' does not exist!");
                } else {
                    plugin.getLogger().info("Discovered resource pack '" + name + "'");
                }
                resourcepacks.put(key, file);
            }
        } else {
            plugin.getLogger().warning(ChatColor.RED + "There are no resource packs listed in the config!");
        }
    }

}
