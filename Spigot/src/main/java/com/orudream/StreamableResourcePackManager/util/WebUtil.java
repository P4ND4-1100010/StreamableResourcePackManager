package com.orudream.StreamableResourcePackManager.util;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Arrays;

import com.orudream.StreamableResourcePackManager.httpd.MineHttpd;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;


public class WebUtil {
	public static Player getAddress( MineHttpd.MineConnection connection ) {
		byte[] mac = connection.getClient().getInetAddress().getAddress();
		for ( Player player : Bukkit.getOnlinePlayers() ) {
			if ( Arrays.equals( player.getAddress().getAddress().getAddress(), mac ) ) {
				return player;
			}
		}
		return null;
	}
	
	public static byte[] getMAC( InetAddress address ) {
		try {
			return NetworkInterface.getByInetAddress( address ).getHardwareAddress();
		} catch ( SocketException e ) {
			e.printStackTrace();
			return null;
		}
	}
}
